import asyncio
import logging
import aiosqlite
import bleach
from cryptography.fernet import Fernet
from pytube import YouTube
from summa import summarizer
from llama_cpp import Llama
from textblob import TextBlob
import numpy as np
import pennylane as qml

class App:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        fh = logging.FileHandler('app.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

        self.llm = None
        self.db_name = "llama_database.db"
        self.fernet_key = Fernet.generate_key()
        self.fernet = Fernet(self.fernet_key)

        self.youtube_links = [
            "https://www.youtube.com/watch?v=dQw4w9WgXcQ",  # Example links, replace with actual links
            "https://www.youtube.com/watch?v=oHg5SJYRHA0",
            "https://www.youtube.com/watch?v=ScMzIvxBSi4",
            # Add more YouTube links as needed
        ]

    def run(self):
        try:
            self.setup()
            asyncio.run(self.process_links())
        except Exception as e:
            self.logger.error(f"Error running the application: {e}")

    def setup(self):
        try:
            self.load_model()
            self.initialize_database()
        except Exception as e:
            self.logger.error(f"Error during setup: {e}")
            raise

    def load_model(self):
        model_name = "llama-2-7b-chat.ggmlv3.q8_0.bin"  # Replace with your Llama model path
        self.logger.info(f"Loading model: {model_name}")
        try:
            self.llm = Llama(
                model_path=model_name,
                n_gpu_layers=-1,
                n_ctx=3900,
            )
            self.logger.info("Model loaded successfully")
        except Exception as e:
            self.logger.error(f"Error loading the model: {e}")
            raise

    async def initialize_database(self):
        try:
            async with aiosqlite.connect(self.db_name) as db:
                await db.execute('''CREATE TABLE IF NOT EXISTS entries (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    link TEXT NOT NULL,
                                    title TEXT NOT NULL,
                                    description TEXT NOT NULL,
                                    sentiment_amplitude REAL NOT NULL,
                                    quantum_state TEXT NOT NULL
                                )''')
                await db.commit()
                self.logger.info("Database initialized successfully")
        except Exception as e:
            self.logger.error(f"Error initializing database: {e}")
            raise

    async def save_to_database(self, link, title, description, sentiment_amplitude, quantum_state):
        try:
            self.logger.info(f"Saving data to database for link: {link}")
            async with aiosqlite.connect(self.db_name) as db:
                await db.execute("INSERT INTO entries (link, title, description, sentiment_amplitude, quantum_state) VALUES (?, ?, ?, ?, ?)",
                                (bleach.clean(link), bleach.clean(title), bleach.clean(description), sentiment_amplitude, bleach.clean(quantum_state)))
                await db.commit()
        except Exception as e:
            self.logger.error(f"Error saving data to database for link {link}: {e}")

    def process_links(self):
        for link in self.youtube_links:
            try:
                title, description = self.get_youtube_metadata(link)
                summarized_description = self.summarize_description(description)
                sentiment_amplitude = self.sentiment_to_amplitude(summarized_description)
                quantum_state = self.offline_quantum_circuit_analysis(summarized_description, sentiment_amplitude)
                response = self.process_llama_response(link, title, summarized_description, sentiment_amplitude, quantum_state)
                self.logger.info(f"Processing link: {link} - Response: {response}")
                asyncio.run(self.save_to_database(link, title, summarized_description, sentiment_amplitude, quantum_state))
            except Exception as e:
                self.logger.error(f"Error processing link {link}: {e}")

    def get_youtube_metadata(self, link):
        try:
            yt = YouTube(link)
            title = yt.title
            description = yt.description
            return title, description
        except Exception as e:
            self.logger.error(f"Error fetching metadata for link {link}: {e}")
            raise

    def summarize_description(self, description):
        try:
            summarized = summarizer.summarize("[youtubedescription] " + description)
            return summarized
        except Exception as e:
            self.logger.error(f"Error summarizing description: {e}")
            raise

    def sentiment_to_amplitude(self, text):
        try:
            analysis = TextBlob(text)
            return (analysis.sentiment.polarity + 1) / 2  # Normalize sentiment polarity to [0, 1]
        except Exception as e:
            self.logger.error(f"Error converting sentiment to amplitude: {e}")
            raise

    def offline_quantum_circuit_analysis(self, description, sentiment_amplitude):
        try:
            dev = qml.device("default.qubit", wires=4)

            @qml.qnode(dev)
            def circuit(color_code, amplitude):
                r, g, b = [int(color_code[i:i+2], 16) for i in (1, 3, 5)]
                r, g, b = r / 255.0, g / 255.0, b / 255.0
                qml.RY(r * np.pi, wires=0)
                qml.RY(g * np.pi, wires=1)
                qml.RY(b * np.pi, wires=2)
                qml.RY(amplitude * np.pi, wires=3)
                qml.CNOT(wires=[0, 1])
                qml.CNOT(wires=[1, 2])
                qml.CNOT(wires=[2, 3])
                return qml.probs(wires=[0, 1, 2, 3])

            color_code = self.generate_color_code(description)
            return circuit(color_code, sentiment_amplitude)
        except Exception as e:
            self.logger.error(f"Error in offline quantum circuit analysis: {e}")
            raise

    def generate_color_code(self, description):
        try:
            analysis = TextBlob(description)
            sentiment = analysis.sentiment.polarity  # Range: [-1, 1]
            red = int((sentiment + 1) * 127.5)  # Map sentiment to [0, 255]
            green = 255 - red  # Inverse for complementary color
            blue = 0
            color_code = f"#{red:02x}{green:02x}{blue:02x}"
            return color_code
        except Exception as e:
            self.logger.error(f"Error generating color code: {e}")
            raise

    def process_llama_response(self, link, title, description, sentiment_amplitude, quantum_state):
        try:
            prompt = (
                f"[pastcontext]YouTube link metadata retrieved: Title - {title}, Description - {description}, Sentiment Amplitude - {sentiment_amplitude}, Quantum State - {quantum_state}[/pastcontext] "
                "[action: Evaluate] "
                "[task: Check YouTube Link Safety for Children] "
                f"[request: Check YouTube Link Safety for {link}] "
                "[responsetemplate] Is the YouTube link SAFE or UNSAFE?[/responsetemplate]"
            )

            llama_output = self.llm(prompt, max_tokens=459)
            if 'choices' in llama_output and isinstance(llama_output['choices'], list) and len(llama_output['choices']) > 0:
                output_text = llama_output['choices'][0].get('text', '').strip()
                self.logger.info(f"Llama Output: {output_text}")

                # Process the response to get one-word reply
                if 'SAFE' in output_text and 'UNSAFE' not in output_text:
                    return 'SAFE'
                elif 'UNSAFE' in output_text and 'SAFE' not in output_text:
                    return 'UNSAFE'
                else:
                    # If both SAFE and UNSAFE are present, slightly modify the prompt and retry
                    prompt = (
                        f"[pastcontext]YouTube link metadata retrieved: Title - {title}, Description - {description}, Sentiment Amplitude - {sentiment_amplitude}, Quantum State - {quantum_state}[/pastcontext] "
                        "[action: Evaluate] "
                        "[task: Check YouTube Link Safety for Children] "
                        f"[request: Check YouTube Link Safety for {link}] "
                        "[responsetemplate] Please provide a clear response: Is the YouTube link SAFE or UNSAFE?[/responsetemplate]"
                    )
                    llama_output = self.llm(prompt, max_tokens=459)
                    if 'choices' in llama_output and isinstance(llama_output['choices'], list) and len(llama_output['choices']) > 0:
                        output_text = llama_output['choices'][0].get('text', '').strip()
                        self.logger.info(f"Llama Output (Retry): {output_text}")
                        if 'SAFE' in output_text and 'UNSAFE' not in output_text:
                            return 'SAFE'
                        elif 'UNSAFE' in output_text and 'SAFE' not in output_text:
                            return 'UNSAFE'
                        else:
                            self.logger.error(f"Failed to determine safety status for YouTube link: {link}. Ambiguous response.")
                            return "Ambiguous"
                    else:


                        .logger.error(f"Failed to determine safety status for YouTube link: {link}. No valid response.")
                        return "No Response"
            else:
                self.logger.error(f"Failed to get a proper response for YouTube link: {link}.")
                return "Failed to get a proper response."
        except Exception as e:
            self.logger.error(f"Error processing llama response for link {link}: {e}")
            return "Error"

if __name__ == "__main__":
    app = App()
    app.run()